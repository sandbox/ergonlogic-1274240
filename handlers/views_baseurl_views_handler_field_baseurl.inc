<?php
/**
 * A handler to provide a field that is the base URL of the site.
 */
class views_baseurl_views_handler_field_baseurl extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['baseurl']['#title'] = 'Base URL';
    $form['baseurl']['#type'] = 'textfield';
    $form['baseurl']['#disabled'] = TRUE;
    $form['baseurl']['#description'] = 'The base URL of the site.';
    $form['baseurl']['#value'] = $GLOBALS['base_url'];
    
    // Remove the rest of the form options
    unset($form['alter']);
    unset($form['hide_empty']);
    unset($form['empty_zero']);
    unset($form['empty']);
 }

  function render($values) {
    return $GLOBALS['base_url'];
  }
}
